		GNOME Plugin Specification

Programs may wish to load CORBA servers from shared object files using
the dlopen() interface (or whatever dynamic linker API is available on
the system).  This document specifies the procedure that GNOME
programs will use to load, activate, use, and deactivate servers
stored in shared objects.

* Where to look for shared objects

The directory $(prefix)/lib/CORBA/servers is suggested as a standard
directory for storing the shared object files in. Filenames should be
as specific as possible to avoid namespace clashes (e.g. use
"gnome-speller-ispell-1.0.so" instead of "spell.so").

* What to do with a shared object

dlopen() it. A variable with the signature "const GNOME_Plugin
*GNOME_Plugins_list;" will be defined. It should be a
pointer to an array of plugin object information structures.

* Plugin object information
typedef struct {
	const char *repo_id, *id, *kind, *description;
	CORBA_Object (*activate)(PortableServer_POA poa,
				 gpointer *impl_ptr,
				 CORBA_Environment *ev);
	void (*deactivate)(PortableServer_POA poa,
			   gpointer impl_ptr,
			   CORBA_Environment *ev);
} GNOME_Plugin;

"kind" may be NULL.
A "NULL" repo_id field means end-of-array.

The "id" and "description" fields are C strings of completely
arbitrary contents, but it is recommended that they contain a short
identifying string and a human-readable description, respectively.

** "activate" method of a plugin
Calling this function will return a CORBA_Object for the server.
"impl_ptr" should be a pointer to a pointer variable. This is only used
to pass to the "deactivate" method.

** Using it
Make CORBA calls on the CORBA_Object as normal. Make sure not to
release() the object reference until you're ready to deactivate the
object (so that same-address-space calls will happen).

** Losing it ("deactivate")
Call this function when you're done using the object and want to
destroy the server (pass in the impl_ptr that was returned by the
"activate" method).

-------------------------------
Elliot Lee <sopwith@redhat.com>