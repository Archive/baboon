#include "baboons.h"
#include <pwd.h>
#include <grp.h>
#include <sys/types.h>
#include <limits.h>
#include <dirent.h>
#include <vfs.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

/* Custom servants */
typedef struct{
  POA_baboon_StorageFactory servant;
  PortableServer_POA poa;
  PortableServer_ObjectId *objid;
} impl_POA_baboon_StorageFactory;

typedef struct {
  POA_baboon_StorageDirectory servant;
  PortableServer_POA poa;
  PortableServer_ObjectId *objid;

  baboon_Storage_DEType type;
  CORBA_char *path;
  int fd;
  DIR *dir;
} impl_POA_baboon_Storage;

typedef impl_POA_baboon_Storage impl_POA_baboon_DataStore;
typedef impl_POA_baboon_Storage impl_POA_baboon_StorageDirectory;

/**************************
 * Forward declarations   *
 **************************/

/* StorageFactory */
baboon_DataStore
server_baboon_StorageFactory_open(impl_POA_baboon_StorageFactory *servant,
				  CORBA_char *path,
				  baboon_Storage_OpenMode flags,
				  CORBA_Environment *ev);
baboon_StorageDirectory
server_baboon_StorageFactory_opendir(impl_POA_baboon_StorageFactory *servant,
				     CORBA_char *path,
				     CORBA_Environment *ev);
void server_baboon_StorageFactory_chmod(impl_POA_baboon_StorageFactory *servant,
					CORBA_char * path,
					CORBA_long mode,
					CORBA_Environment * ev);
void server_baboon_StorageFactory_chown(impl_POA_baboon_StorageFactory *servant,
					CORBA_char * path,
					CORBA_char * owner,
					CORBA_char * group,
					CORBA_Environment * ev);
CORBA_char *server_baboon_StorageFactory_readlink(impl_POA_baboon_StorageFactory *servant,
						  CORBA_char * path,
						  CORBA_Environment * ev);
void server_baboon_StorageFactory_unlink(impl_POA_baboon_StorageFactory *servant,
					 CORBA_char * path,
					 CORBA_Environment * ev);

void server_baboon_StorageFactory_symlink(impl_POA_baboon_StorageFactory *servant,
					  CORBA_char * existingpath,
					  CORBA_char * topath,
					  CORBA_Environment *ev);

void server_baboon_StorageFactory_rename(impl_POA_baboon_StorageFactory *servant,
					 CORBA_char * existingpath,
					 CORBA_char * topath,
					 CORBA_Environment * ev);
/* Storage */
baboon_Storage_DEType
servant_baboon_Storage__get_type(impl_POA_baboon_Storage *servant,
				 CORBA_Environment * ev);

CORBA_char *
servant_baboon_Storage__get_path(impl_POA_baboon_Storage *servant,
				 CORBA_Environment * ev);

void
servant_baboon_Storage_close(impl_POA_baboon_Storage *servant,
			     CORBA_Environment * ev);

/* StorageDirectory */
baboon_Storage_dirent *
server_baboon_StorageDirectory_readdir(impl_POA_baboon_StorageDirectory *servant,
				       CORBA_Environment * ev);

baboon_StorageDirectory_DirPosition
server_baboon_StorageDirectory_telldir(impl_POA_baboon_StorageDirectory *servant,
				       CORBA_Environment * ev);

void
server_baboon_StorageDirectory_seekdir(impl_POA_baboon_StorageDirectory *servant,
				       baboon_StorageDirectory_DirPosition offset,
				       CORBA_Environment *
ev);
/* DataStore */

baboon_Data *
server_baboon_DataStore_read(impl_POA_baboon_DataStore *servant,
			     CORBA_long length,
			     CORBA_Environment * ev);

CORBA_long
server_baboon_DataStore_write(impl_POA_baboon_DataStore *servant,
			      baboon_Data * buffer,
			      CORBA_Environment * ev);

CORBA_long
server_baboon_DataStore_lseek(impl_POA_baboon_DataStore *servant,
			      CORBA_long offset,
			      baboon_Storage_PositionRelator whence,
			      CORBA_Environment * ev);

/**************************
 * Structures for the POA *
 **************************/
PortableServer_ServantBase__epv impl_base_epv = {
  NULL,
  NULL, /* finalize */
  NULL, /* default POA */
};

POA_baboon_StorageFactory__epv impl_baboon_StorageFactory_epv = {
  NULL,
  (gpointer)&server_baboon_StorageFactory_open,
  (gpointer)&server_baboon_StorageFactory_opendir,
  (gpointer)&server_baboon_StorageFactory_chmod,
  (gpointer)&server_baboon_StorageFactory_chown,
  (gpointer)&server_baboon_StorageFactory_readlink,
  (gpointer)&server_baboon_StorageFactory_unlink,
  (gpointer)&server_baboon_StorageFactory_symlink,
  (gpointer)&server_baboon_StorageFactory_rename,
};

POA_baboon_StorageFactory__vepv impl_baboon_StorageFactory_vepv = {
  &impl_base_epv,
  &impl_baboon_StorageFactory_epv
};

POA_baboon_Storage__epv impl_baboon_Storage_epv = {
  NULL,
  (gpointer)&servant_baboon_Storage__get_type,
  (gpointer)&servant_baboon_Storage__get_path,
  (gpointer)&servant_baboon_Storage_close,
};

POA_baboon_DataStore__epv impl_baboon_DataStore_epv = {
  NULL,
  (gpointer)&server_baboon_DataStore_read,
  (gpointer)&server_baboon_DataStore_write,
  (gpointer)&server_baboon_DataStore_lseek
};

POA_baboon_StorageDirectory__epv impl_baboon_StorageDirectory_epv = {
  NULL,
  (gpointer)&server_baboon_StorageDirectory_readdir,
  (gpointer)&server_baboon_StorageDirectory_telldir,
  (gpointer)&server_baboon_StorageDirectory_seekdir
};


POA_baboon_StorageDirectory__vepv impl_baboon_StorageDirectory_vepv = {
  &impl_base_epv,
  &impl_baboon_Storage_epv,
  &impl_baboon_StorageDirectory_epv
};

/* Implementations */
baboon_StorageFactory 
baboon_StorageFactory__create(PortableServer_POA poa,
			      PortableServer_Servant *servant,
			      CORBA_Environment * ev)
{
   baboon_StorageFactory retval;
   impl_POA_baboon_StorageFactory *newservant;
   PortableServer_ObjectId *objid;

   newservant = g_new0(impl_POA_baboon_StorageFactory, 1);
   newservant->servant.vepv = &impl_baboon_StorageFactory_vepv;
   newservant->poa = poa;
   POA_baboon_StorageFactory__init((PortableServer_Servant) newservant, ev);
   objid = PortableServer_POA_activate_object(poa, newservant, ev);
   newservant->objid = objid;
   retval = PortableServer_POA_servant_to_reference(poa, newservant, ev);

   if(servant)
     *servant = newservant;

   return retval;
}

void
baboon_StorageFactory__destroy(PortableServer_Servant servant,
			       CORBA_Environment *ev)
{
  impl_POA_baboon_StorageFactory *old = servant;

  PortableServer_POA_deactivate_object(old->poa, old->objid, ev);
  CORBA_free(old->objid);
  POA_baboon_StorageFactory__fini(servant, ev);
  g_free(servant);
}

baboon_DataStore
server_baboon_StorageFactory_open(impl_POA_baboon_StorageFactory *servant,
				  CORBA_char *path,
				  baboon_Storage_OpenMode flags,
				  CORBA_Environment *ev)
{
  impl_POA_baboon_DataStore *newservant;
  int fd;

  fd = mc_open(path, flags);

  if(fd < 0)
    return CORBA_OBJECT_NIL;

  newservant = g_new0(impl_POA_baboon_DataStore, 1);

  POA_baboon_DataStore__init((PortableServer_Servant)newservant, ev);

  newservant->type = baboon_Storage_DEDATA;
  newservant->fd = fd;
  newservant->path = CORBA_string_dup(path);

  newservant->poa = servant->poa;
  newservant->objid = PortableServer_POA_activate_object(servant->poa,
							 (PortableServer_Servant)newservant,
							 ev);

  return PortableServer_POA_servant_to_reference(servant->poa,
						 newservant, ev);
}

baboon_StorageDirectory
server_baboon_StorageFactory_opendir(impl_POA_baboon_StorageFactory *servant,
				     CORBA_char *path,
				     CORBA_Environment *ev)
{
  impl_POA_baboon_StorageDirectory *newservant;
  DIR *dir;

  dir = mc_opendir(path);

  if(!dir)
    return CORBA_OBJECT_NIL;

  newservant = g_new0(impl_POA_baboon_StorageDirectory, 1);
  newservant->servant.vepv = &impl_baboon_StorageDirectory_vepv;

  POA_baboon_StorageDirectory__init((PortableServer_Servant)newservant, ev);

  newservant->dir = dir;

  newservant->poa = servant->poa;
  newservant->objid = PortableServer_POA_activate_object(servant->poa,
							 (PortableServer_Servant)newservant,
							 ev);
  return PortableServer_POA_servant_to_reference(servant->poa,
						 newservant, ev);  
}

void
server_baboon_StorageFactory_chmod(impl_POA_baboon_StorageFactory *servant,
				   CORBA_char * path,
				   CORBA_long mode,
				   CORBA_Environment * ev)
{
  mc_chmod(path, mode);
}

void
server_baboon_StorageFactory_chown(impl_POA_baboon_StorageFactory *servant,
				   CORBA_char * path,
				   CORBA_char * owner,
				   CORBA_char * group,
				   CORBA_Environment * ev)
{
  struct passwd *pwent;
  struct group *grent;
  int new_owner = -1, new_group = -1;

  if(owner && *owner) {
    pwent = getpwnam(owner);
    if(pwent)
      new_owner = pwent->pw_uid;
  }

  if(group && *group) {
    grent = getgrnam(group);
    if(grent)
      new_group = grent->gr_gid;
  }

  mc_chown(path, new_owner, new_group);
}

CORBA_char *
server_baboon_StorageFactory_readlink(impl_POA_baboon_StorageFactory *servant,
				      CORBA_char * path,
				      CORBA_Environment * ev)
{
  CORBA_char *buf;

  buf = CORBA_string_alloc(_POSIX_PATH_MAX);

  if(mc_readlink(path, buf, _POSIX_PATH_MAX)) {
    CORBA_free(buf); buf = NULL;
  }

  return buf;
}

void
server_baboon_StorageFactory_unlink(impl_POA_baboon_StorageFactory *servant,
				    CORBA_char * path,
				    CORBA_Environment * ev)
{
  mc_unlink(path);
}

void
server_baboon_StorageFactory_symlink(impl_POA_baboon_StorageFactory *servant,
				     CORBA_char * existingpath,
				     CORBA_char * topath,
				     CORBA_Environment *ev)
{
  mc_symlink(existingpath, topath);
}

void
server_baboon_StorageFactory_rename(impl_POA_baboon_StorageFactory *servant,
				    CORBA_char * existingpath,
				    CORBA_char * topath,
				    CORBA_Environment * ev)
{
  mc_rename(existingpath, topath);
}

/* Storage */
baboon_Storage_DEType
servant_baboon_Storage__get_type(impl_POA_baboon_Storage *servant,
				 CORBA_Environment * ev)
{
  return servant->type;
}

CORBA_char *
servant_baboon_Storage__get_path(impl_POA_baboon_Storage *servant,
				 CORBA_Environment * ev)
{
  return CORBA_string_dup(servant->path);
}

void
servant_baboon_Storage_close(impl_POA_baboon_Storage *servant,
			     CORBA_Environment * ev)
{
  /* "destroyme" */
  PortableServer_POA_deactivate_object(servant->poa, servant->objid, ev);

  switch(servant->type) {
  case baboon_Storage_DEDATA:
    mc_close(servant->fd);
    break;
  case baboon_Storage_DEDIR:
    mc_closedir(servant->dir);
    break;
  default:
    g_error("How did this servant become existent?");
  }

  CORBA_free(servant->path);
  
  POA_baboon_Storage__fini((PortableServer_Servant)servant, ev);
  g_free(servant);
}

/* StorageDirectory */
baboon_Storage_dirent *
server_baboon_StorageDirectory_readdir(impl_POA_baboon_StorageDirectory *servant,
				       CORBA_Environment * ev)
{
  baboon_Storage_dirent *retval = NULL;
  struct dirent *dent;

  retval = baboon_Storage_dirent__alloc();
  dent = mc_readdir(servant->dir);

  if(dent) {
    switch(dent->d_type) {
    case DT_DIR:
      retval->type = baboon_Storage_DEDIR;
      break;
    case DT_LNK:
      retval->type = baboon_Storage_DELINK;
      break;
    default:
      retval->type = baboon_Storage_DEDATA;
    }

    retval->name = CORBA_string_dup(dent->d_name);
  } else {
    retval->type = baboon_Storage_DEEND;
    retval->name = NULL;
  }

  return retval;
}

baboon_StorageDirectory_DirPosition
server_baboon_StorageDirectory_telldir(impl_POA_baboon_StorageDirectory *servant,
				       CORBA_Environment * ev)
{
  return (baboon_StorageDirectory_DirPosition)mc_telldir(servant->dir);
}

void
server_baboon_StorageDirectory_seekdir(impl_POA_baboon_StorageDirectory *servant,
				       baboon_StorageDirectory_DirPosition offset,
				       CORBA_Environment *ev)
{
  mc_seekdir(servant->dir, offset);
}

/* DataStore */

baboon_Data *
server_baboon_DataStore_read(impl_POA_baboon_DataStore *servant,
			     CORBA_long length,
			     CORBA_Environment * ev)
{
  baboon_Data *retval = baboon_Data__alloc();

  retval->_buffer = CORBA_octet_allocbuf(length);
  CORBA_sequence_set_release(retval, CORBA_TRUE);
  retval->_length = mc_read(servant->fd, retval->_buffer, length);

  return retval;
}

CORBA_long
server_baboon_DataStore_write(impl_POA_baboon_DataStore *servant,
			      baboon_Data * buffer,
			      CORBA_Environment * ev)
{
  return mc_write(servant->fd, buffer->_buffer, buffer->_length);
}

CORBA_long
server_baboon_DataStore_lseek(impl_POA_baboon_DataStore *servant,
			      CORBA_long offset,
			      baboon_Storage_PositionRelator whence,
			      CORBA_Environment * ev)
{
  int mcwhence;

  switch(whence) {
  case baboon_Storage_SET:
    mcwhence = SEEK_SET;
    break;
  case baboon_Storage_CUR:
    mcwhence = SEEK_CUR;
    break;
  case baboon_Storage_END:
    mcwhence = SEEK_END;
    break;
  default:
    g_warning("Unknown PositionRelator %d", whence);
    return -1;
  }

  return mc_lseek(servant->fd, offset, mcwhence);
}
