#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <signal.h>
#include <gdk/gdkx.h>
#include <gnome.h>
#include <libgnorba/gnorba.h>

#define SERVER_ACTIVATION_TIMEOUT 15

/*** App-specific servant structures ***/

typedef struct {
  POA_CosNaming_NamingContext servant;
  PortableServer_POA poa;
  PortableServer_ObjectId *objid;

  GHashTable *registered_names;
} impl_POA_CosNaming_NamingContext;

typedef struct {
  char *id, *kind;
  enum RegistrationType { OBJECT_ACTIVE, OBJECT_INACTIVE, CTX_REMOTE } type;
  union {
    struct {
      CORBA_Object active_object;
      char *inactive_cmdline;
    } obj;
    CORBA_Object ctx;
  } u;
} RegisteredName;

typedef struct {
  POA_CosNaming_BindingIterator servant;
  PortableServer_POA poa;

  GList *items, *curitem;
} impl_POA_CosNaming_BindingIterator;

/*** Implementation stub prototypes ***/

static void impl_CosNaming_NamingContext__destroy(impl_POA_CosNaming_NamingContext * servant,
						  CORBA_Environment * ev);

void
impl_CosNaming_NamingContext_bind(impl_POA_CosNaming_NamingContext * servant,
				  CosNaming_Name * n,
				  CORBA_Object obj,
				  CORBA_Environment * ev);

void
impl_CosNaming_NamingContext_rebind(impl_POA_CosNaming_NamingContext * servant,
				    CosNaming_Name * n,
				    CORBA_Object obj,
				    CORBA_Environment * ev);

void
impl_CosNaming_NamingContext_bind_context(impl_POA_CosNaming_NamingContext * servant,
					  CosNaming_Name * n,
					  CosNaming_NamingContext nc,
					  CORBA_Environment * ev);

void
impl_CosNaming_NamingContext_rebind_context(impl_POA_CosNaming_NamingContext * servant,
					    CosNaming_Name * n,
					    CosNaming_NamingContext nc,
					    CORBA_Environment * ev);

CORBA_Object
impl_CosNaming_NamingContext_resolve(impl_POA_CosNaming_NamingContext * servant,
				     CosNaming_Name * n,
				     CORBA_Environment * ev);

void
impl_CosNaming_NamingContext_unbind(impl_POA_CosNaming_NamingContext * servant,
				    CosNaming_Name * n,
				    CORBA_Environment * ev);

CosNaming_NamingContext
impl_CosNaming_NamingContext_new_context(impl_POA_CosNaming_NamingContext * servant,
					 CORBA_Environment * ev);

CosNaming_NamingContext
impl_CosNaming_NamingContext_bind_new_context(impl_POA_CosNaming_NamingContext * servant,
					      CosNaming_Name * n,
					      CORBA_Environment * ev);

void
impl_CosNaming_NamingContext_destroy(impl_POA_CosNaming_NamingContext * servant,
				     CORBA_Environment * ev);

void
impl_CosNaming_NamingContext_list(impl_POA_CosNaming_NamingContext * servant,
				  CORBA_unsigned_long how_many,
				  CosNaming_BindingList ** bl,
				  CosNaming_BindingIterator * bi,
				  CORBA_Environment * ev);

#if 0
static void impl_CosNaming_BindingIterator__destroy(impl_POA_CosNaming_BindingIterator * servant,
						    CORBA_Environment * ev);

CORBA_boolean
impl_CosNaming_BindingIterator_next_one(impl_POA_CosNaming_BindingIterator * servant,
					CosNaming_Binding ** b,
					CORBA_Environment * ev);

CORBA_boolean
impl_CosNaming_BindingIterator_next_n(impl_POA_CosNaming_BindingIterator * servant,
				      CORBA_unsigned_long how_many,
				      CosNaming_BindingList ** bl,
				      CORBA_Environment * ev);

void
impl_CosNaming_BindingIterator_destroy(impl_POA_CosNaming_BindingIterator * servant,
				       CORBA_Environment * ev);
#endif

/*** epv structures ***/

static PortableServer_ServantBase__epv impl_CosNaming_NamingContext_base_epv =
{
  NULL,			/* _private data */
  (gpointer) & impl_CosNaming_NamingContext__destroy,	/* finalize routine */
  NULL,			/* default_POA routine */
};
static POA_CosNaming_NamingContext__epv impl_CosNaming_NamingContext_epv =
{
  NULL,			/* _private */

  (gpointer) & impl_CosNaming_NamingContext_bind,

  (gpointer) & impl_CosNaming_NamingContext_rebind,

  (gpointer) & impl_CosNaming_NamingContext_bind_context,

  (gpointer) & impl_CosNaming_NamingContext_rebind_context,

  (gpointer) & impl_CosNaming_NamingContext_resolve,

  (gpointer) & impl_CosNaming_NamingContext_unbind,

  (gpointer) & impl_CosNaming_NamingContext_new_context,

  (gpointer) & impl_CosNaming_NamingContext_bind_new_context,

  (gpointer) & impl_CosNaming_NamingContext_destroy,

  (gpointer) & impl_CosNaming_NamingContext_list,

};

#if 0
static PortableServer_ServantBase__epv impl_CosNaming_BindingIterator_base_epv =
{
  NULL,			/* _private data */
  (gpointer) & impl_CosNaming_BindingIterator__destroy,	/* finalize routine */
  NULL,			/* default_POA routine */
};
static POA_CosNaming_BindingIterator__epv impl_CosNaming_BindingIterator_epv =
{
  NULL,			/* _private */
  (gpointer) & impl_CosNaming_BindingIterator_next_one,

  (gpointer) & impl_CosNaming_BindingIterator_next_n,

  (gpointer) & impl_CosNaming_BindingIterator_destroy,

};
#endif

/*** vepv structures ***/

static POA_CosNaming_NamingContext__vepv impl_CosNaming_NamingContext_vepv =
{
  &impl_CosNaming_NamingContext_base_epv,
  &impl_CosNaming_NamingContext_epv,
};
#if 0
static POA_CosNaming_BindingIterator__vepv impl_CosNaming_BindingIterator_vepv =
{
  &impl_CosNaming_BindingIterator_base_epv,
  &impl_CosNaming_BindingIterator_epv,
};
#endif

/*** Stub implementations ***/

static CosNaming_NamingContext 
impl_CosNaming_NamingContext__create(PortableServer_POA poa,
				     impl_POA_CosNaming_NamingContext **servant,
				     CORBA_Environment * ev)
{
  CosNaming_NamingContext retval;
  impl_POA_CosNaming_NamingContext *newservant;
  PortableServer_ObjectId *objid;

  newservant = g_new0(impl_POA_CosNaming_NamingContext, 1);
  newservant->servant.vepv = &impl_CosNaming_NamingContext_vepv;
  newservant->poa = poa;
  newservant->registered_names = g_hash_table_new(g_str_hash, g_str_equal);

  POA_CosNaming_NamingContext__init((PortableServer_Servant) newservant, ev);
  objid = PortableServer_POA_activate_object(poa, newservant, ev);
  newservant->objid = objid;
  retval = PortableServer_POA_servant_to_reference(poa, newservant, ev);

  if(servant)
    *servant = newservant;

  return retval;
}

/* You shouldn't call this routine directly without first deactivating the servant... */
static void
impl_CosNaming_NamingContext__destroy(impl_POA_CosNaming_NamingContext * servant, CORBA_Environment * ev)
{
  /* XXX Do the foreach here... */
  g_hash_table_destroy(servant->registered_names);
  POA_CosNaming_NamingContext__fini((PortableServer_Servant) servant, ev);
  g_free(servant);
}

static void
do_bind(impl_POA_CosNaming_NamingContext *servant,
	CosNaming_Name *n,
	int level,
	enum RegistrationType rtype,
	CORBA_boolean is_rebind,
	gpointer obj,
	CORBA_Environment *ev)
{
  RegisteredName *rn;
  CosNaming_NameComponent *nc;
  CosNaming_Name tmpn;

  g_return_if_fail(level < n->_length);
  nc = &n->_buffer[level];

  rn = g_hash_table_lookup(servant->registered_names, nc->id);

  if(level >= (n->_length - 1)) {
    if(is_rebind) {
      if(!rn) {
	g_message("Rebind on [%s, %s] failed - does not exist",
		  nc->id, nc->kind);
	CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			    ex_CosNaming_NamingContext_NotFound, NULL);
	return;
      }
      if(rn->type != rtype
	 && (rn->type != OBJECT_INACTIVE && rtype != OBJECT_ACTIVE)) {
	g_message("Rebind on [%s, %s] failed - binding types don't match",
		  nc->id, nc->kind);
	CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			    ex_CosNaming_NamingContext_CannotProceed, NULL);
	return;
      }
    };

    if(!is_rebind && rn) {
      g_message("bind on [%s, %s] failed - already bound",
		  nc->id, nc->kind);
      CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			  ex_CosNaming_NamingContext_AlreadyBound, NULL);
      return;
    }

    /* current context is what we want to perform this operation in*/
    if(rn) {
      switch(rn->type) {
      case OBJECT_ACTIVE:
	g_message("rebinding object for [%s, %s]",
		  nc->id, nc->kind);
	CORBA_Object_release(rn->u.obj.active_object, ev);
      case OBJECT_INACTIVE:
	if(rtype == OBJECT_ACTIVE)
	  g_message("Activating object for [%s, %s]",
		    nc->id, nc->kind);
	break;
      case CTX_REMOTE:
	g_message("rebinding context for [%s, %s]",
		  nc->id, nc->kind);
	CORBA_Object_release(rn->u.ctx, ev);
	break;
      }
      g_free(rn->id);
      g_free(rn->kind);
    } else {
      rn = g_new0(RegisteredName, 1);
      g_message("Adding new binding for [%s, %s]",
		nc->id, nc->kind);
    }
    rn->id = g_strdup(nc->id);
    rn->kind = g_strdup(nc->id);

    switch(rtype) {
    case OBJECT_INACTIVE:
      g_warning("Trying to rebind an inactive! this should not happen!");
      return; /* XXX theoretical memleak */
    case OBJECT_ACTIVE:
      rn->u.obj.active_object = CORBA_Object_duplicate(obj, ev);
      break;
    case CTX_REMOTE:
      rn->u.ctx = CORBA_Object_duplicate(obj, ev);
      break;
    }
    rn->type = rtype;
    g_hash_table_insert(servant->registered_names, rn->id, rn);
  } else {
    if(!rn) {
      CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			  ex_CosNaming_NamingContext_NotFound, NULL);
      return;
    };

    switch(rn->type) {
    case CTX_REMOTE:
      tmpn = *n;
      tmpn._length--;
      tmpn._buffer++;
      switch(rtype) {
      case OBJECT_INACTIVE:
	return; /* XXX memleak */
      case OBJECT_ACTIVE:
	if(is_rebind)
	  CosNaming_NamingContext_rebind(rn->u.ctx,
					 &tmpn, obj, ev);
	else
	  CosNaming_NamingContext_bind(rn->u.ctx,
				       &tmpn, obj, ev);
	break;
      case CTX_REMOTE:
	if(is_rebind)
	  CosNaming_NamingContext_rebind_context(rn->u.ctx,
						 &tmpn, obj, ev);
	break;
      }
      break;
    case OBJECT_ACTIVE:
    case OBJECT_INACTIVE:
      CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			  ex_CosNaming_NamingContext_CannotProceed, NULL);
      break;
    }
  }
}

void
do_unbind(impl_POA_CosNaming_NamingContext * servant,
	  CosNaming_Name * n,
	  int level,
	  CORBA_Environment * ev)
{
  RegisteredName *rn;
  CosNaming_NameComponent *nc;
  CosNaming_Name tmpn;

  g_return_if_fail(level < n->_length);
  nc = &n->_buffer[level];

  rn = g_hash_table_lookup(servant->registered_names, nc->id);

  if(!rn) {
    CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			ex_CosNaming_NamingContext_NotFound, NULL);
    return;
  }

  if(level >= (n->_length - 1)) {
    gboolean do_remove = TRUE;

    /* current context is what we want to perform this operation in */
    switch(rn->type) {
    case OBJECT_ACTIVE:
      CORBA_Object_release(rn->u.obj.active_object, ev);
    case OBJECT_INACTIVE:
      if(rn->u.obj.inactive_cmdline) {
	rn->type = OBJECT_INACTIVE;
	do_remove = FALSE;
      }
      break;
    case CTX_REMOTE:
      CORBA_Object_release(rn->u.ctx, ev);
      break;
    }

    if(do_remove) {
      g_hash_table_remove(servant->registered_names,
			  rn->id);
      g_free(rn->id);
      g_free(rn->kind);
      g_free(rn);
    }
  } else {
    switch(rn->type) {
    case CTX_REMOTE:
      tmpn = *n;
      tmpn._length--;
      tmpn._buffer++;
      CosNaming_NamingContext_unbind(rn->u.ctx, &tmpn, ev);
      break;
      break;
    case OBJECT_ACTIVE:
    case OBJECT_INACTIVE:
      CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			  ex_CosNaming_NamingContext_CannotProceed, NULL);
      break;
    }
  }  
}

void
impl_CosNaming_NamingContext_bind(impl_POA_CosNaming_NamingContext * servant,
				  CosNaming_Name * n,
				  CORBA_Object obj,
				  CORBA_Environment * ev)
{
  do_bind(servant, n, 0, OBJECT_ACTIVE,
	  CORBA_FALSE, obj, ev);
}

void
impl_CosNaming_NamingContext_rebind(impl_POA_CosNaming_NamingContext * servant,
				    CosNaming_Name * n,
				    CORBA_Object obj,
				    CORBA_Environment * ev)
{
  do_bind(servant, n, 0, OBJECT_ACTIVE,
	  CORBA_TRUE, obj, ev);
}

void
impl_CosNaming_NamingContext_bind_context(impl_POA_CosNaming_NamingContext * servant,
					  CosNaming_Name * n,
					  CosNaming_NamingContext nc,
					  CORBA_Environment * ev)
{
  do_bind(servant, n, 0, CTX_REMOTE,
	  CORBA_FALSE, nc, ev);
}

void
impl_CosNaming_NamingContext_rebind_context(impl_POA_CosNaming_NamingContext * servant,
					    CosNaming_Name * n,
					    CosNaming_NamingContext nc,
					    CORBA_Environment * ev)
{
  do_bind(servant, n, 0, CTX_REMOTE,
	  CORBA_TRUE, nc, ev);
}

CORBA_Object
do_server_activation(impl_POA_CosNaming_NamingContext *servant,
		     RegisteredName *rn,
		     CORBA_Environment *ev)
{
  int starttime, itmp;
  CORBA_Object retval;
  pid_t pid;

  starttime = time(NULL);

  if((pid = fork())) {
    /* Just loop until the object gets registered, more or less ;-)
       This is a semi-bad hack - to do it correctly we need a wait queue
       and timeouts, but this will do for now.
     */
    alarm(SERVER_ACTIVATION_TIMEOUT);

    while(rn->type != OBJECT_ACTIVE) {
      giop_main_iterate(TRUE);
      if((time(NULL) - starttime) >= SERVER_ACTIVATION_TIMEOUT)
	break;
    }

    alarm(0);

    if(rn->type == OBJECT_ACTIVE) {
      retval = CORBA_Object_duplicate(rn->u.obj.active_object, ev);
    } else {
      retval = CORBA_OBJECT_NIL;
      CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			  ex_CosNaming_NamingContext_NotFound, NULL);
    }
    waitpid(pid, &itmp, 0);
  } else if(fork()) {
    _exit(0); /* de-zombifier */
    /* Terminate thread of execution */
  } else {
    int i, maxfds;

    maxfds = sysconf(_SC_OPEN_MAX);

    for(i = 0; i < maxfds; i++)
      close(i);

    execlp(rn->u.obj.inactive_cmdline, rn->u.obj.inactive_cmdline, NULL);

    _exit(1);
    /* Terminate thread of execution */
  }

  return retval;
}

CORBA_Object
impl_CosNaming_NamingContext_resolve(impl_POA_CosNaming_NamingContext * servant,
				     CosNaming_Name * n,
				     CORBA_Environment * ev)
{
  CORBA_Object retval = CORBA_OBJECT_NIL;
  CosNaming_Name tmpn;
  CosNaming_NameComponent *nc;
  RegisteredName *rn;

  if(n->_length < 1) {
    CORBA_exception_set_system(ev, ex_CORBA_BAD_PARAM, CORBA_COMPLETED_NO);
    return CORBA_OBJECT_NIL;
  }

  nc = &n->_buffer[0];
  rn = g_hash_table_lookup(servant->registered_names, nc->id);

  if(!rn) {
    g_message("Resolution of ID %s (kind %s) failed", nc->id, nc->kind);
    CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			ex_CosNaming_NamingContext_NotFound, NULL);
    return CORBA_OBJECT_NIL;
  }

  if(n->_length == 1) {
    /* This is for us to do locally */
    switch(rn->type) {
    case OBJECT_ACTIVE:
      retval = CORBA_Object_duplicate(rn->u.obj.active_object, ev);
      g_message("Resolution of name [%s, %s] gave %s",
		nc->id, nc->kind, "active object");
      break;
    case OBJECT_INACTIVE:
      do_server_activation(servant, rn, ev);
      if(ev->_major == CORBA_NO_EXCEPTION) {
	retval = CORBA_Object_duplicate(rn->u.obj.active_object, ev);
	g_message("Resolution of name [%s, %s] gave %s",
		  nc->id, nc->kind, "inactive object activated");
      } else
	g_message("Resolution of name [%s, %s] gave exception because server activation failed",
		  nc->id, nc->kind);
      break;
    case CTX_REMOTE:
      retval = CORBA_Object_duplicate(rn->u.ctx, ev);
      g_message("Resolution of name [%s, %s] gave %s",
		nc->id, nc->kind, "subcontext");
      break;
    }
  } else {
    if(rn->type != CTX_REMOTE) {
      CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			  ex_CosNaming_NamingContext_NotFound, NULL);
      return CORBA_OBJECT_NIL;
    }

    tmpn = *n;
    tmpn._length--;
    tmpn._buffer++;

    retval = CosNaming_NamingContext_resolve(rn->u.ctx, &tmpn, ev);
  }

  return retval;
}

void
impl_CosNaming_NamingContext_unbind(impl_POA_CosNaming_NamingContext * servant,
				    CosNaming_Name * n,
				    CORBA_Environment * ev)
{
  do_unbind(servant, n, 0, ev);
}

CosNaming_NamingContext
impl_CosNaming_NamingContext_new_context(impl_POA_CosNaming_NamingContext * servant,
					 CORBA_Environment * ev)
{
  return impl_CosNaming_NamingContext__create(servant->poa, NULL, ev);
}

CosNaming_NamingContext
impl_CosNaming_NamingContext_bind_new_context(impl_POA_CosNaming_NamingContext * servant,
					      CosNaming_Name * n,
					      CORBA_Environment * ev)
{
  CosNaming_NamingContext retval;

  retval = impl_CosNaming_NamingContext__create(servant->poa, NULL, ev);

  do_bind(servant, n, 0, CTX_REMOTE, CORBA_TRUE, retval, ev);

  return retval;
}

void
impl_CosNaming_NamingContext_destroy(impl_POA_CosNaming_NamingContext * servant,
				     CORBA_Environment * ev)
{
  PortableServer_POA_deactivate_object(servant->poa,
				       servant->objid, ev);
  impl_CosNaming_NamingContext__destroy(servant, ev);
}

typedef struct {
  GList *items;
} Binding_Listinfo;

/* This seems very ambiguous - is this supposed to include objects
   inside subcontexts or not? */

void
print_reggedname(gpointer key,
		 RegisteredName *value,
		 impl_POA_CosNaming_NamingContext *servant)
{
  CosNaming_BindingList *l;
  CORBA_Object obj;
  CORBA_Environment ev;

  CORBA_exception_init(&ev);

  g_print("id %s kind %s -> ", value->id, value->kind);
  switch(value->type) {
  case CTX_REMOTE:
    g_print("CONTEXT!\n");
    CosNaming_NamingContext_list(value->u.ctx, 0, &l, &obj, &ev);
    g_print("!ENDCONTEXT");
  case OBJECT_ACTIVE:
    g_print("Active object (exe is %s)",
	    value->u.obj.inactive_cmdline?value->u.obj.inactive_cmdline:"NONE");
    break;
  case OBJECT_INACTIVE:
    g_print("Inactive exe %s)", value->u.obj.inactive_cmdline);
    break;
  }
  g_print("\n");

  CORBA_exception_free(&ev);
}

void
impl_CosNaming_NamingContext_list(impl_POA_CosNaming_NamingContext * servant,
				  CORBA_unsigned_long how_many,
				  CosNaming_BindingList ** bl,
				  CosNaming_BindingIterator * bi,
				  CORBA_Environment * ev)
{
#if 0
  Binding_Listinfo li;

  *bl = CosNaming_BindingList__alloc();
  (*bl)->_length = how_many;
  (*bl)->_buffer = 

  *bi = CORBA_OBJECT_NIL;
#else
  g_message("List NYI, but here's something:");
  g_hash_table_foreach(servant->registered_names, print_reggedname, servant);

  CORBA_exception_set_system(ev, ex_CORBA_NO_IMPLEMENT, CORBA_COMPLETED_NO);
#endif
}

#if 0
static CosNaming_BindingIterator 
impl_CosNaming_BindingIterator__create(PortableServer_POA poa, CORBA_Environment * ev)
{
  CosNaming_BindingIterator retval;
  impl_POA_CosNaming_BindingIterator *newservant;
  PortableServer_ObjectId *objid;

  newservant = g_new0(impl_POA_CosNaming_BindingIterator, 1);
  newservant->servant.vepv = &impl_CosNaming_BindingIterator_vepv;
  newservant->poa = poa;
  POA_CosNaming_BindingIterator__init((PortableServer_Servant) newservant, ev);
  objid = PortableServer_POA_activate_object(poa, newservant, ev);
  CORBA_free(objid);
  retval = PortableServer_POA_servant_to_reference(poa, newservant, ev);

  return retval;
}

/* You shouldn't call this routine directly without first deactivating the servant... */
static void
impl_CosNaming_BindingIterator__destroy(impl_POA_CosNaming_BindingIterator * servant, CORBA_Environment * ev)
{

  POA_CosNaming_BindingIterator__fini((PortableServer_Servant) servant, ev);
  g_free(servant);
}

CORBA_boolean
impl_CosNaming_BindingIterator_next_one(impl_POA_CosNaming_BindingIterator * servant,
					CosNaming_Binding ** b,
					CORBA_Environment * ev)
{
  CORBA_boolean retval;

  return retval;
}

CORBA_boolean
impl_CosNaming_BindingIterator_next_n(impl_POA_CosNaming_BindingIterator * servant,
				      CORBA_unsigned_long how_many,
				      CosNaming_BindingList ** bl,
				      CORBA_Environment * ev)
{
  CORBA_boolean retval;

  return retval;
}

void
impl_CosNaming_BindingIterator_destroy(impl_POA_CosNaming_BindingIterator * servant,
				       CORBA_Environment * ev)
{
}
#endif

static void regen_activation_mapping(void);

void doNothing(void) {}

/* This is where we stick all the object factory activation info :-) */
impl_POA_CosNaming_NamingContext *activation_ctx = NULL;

int main(int argc, char *argv[])
{
  struct sigaction sa;
  gint ior_len;
  
  CORBA_Object obj, actobj;
  char *ior, *chkior;
  int outfd;
  PortableServer_POA poa;
  CORBA_ORB orb;
  CORBA_Environment ev;
  static const CosNaming_NameComponent activation_ctx_name_component =
  {"GNOMEFactories", "GNOMEFactories"};
  static const CosNaming_Name activation_ctx_name =
  {0, 1, (gpointer)&activation_ctx_name_component};
  GdkAtom propname, proptype;

  CORBA_exception_init(&ev);

  orb = gnome_CORBA_init("gnome-name-service", NULL, &argc, argv, 0, NULL, &ev);

#if 0
  gdk_init(&argc, &argv);
#endif

#if 0
  orb = CORBA_ORB_init(&argc, argv, "orbit-local-orb", &ev);
#endif
  poa = (PortableServer_POA)
    CORBA_ORB_resolve_initial_references(orb, "RootPOA", &ev);

  PortableServer_POAManager_activate(PortableServer_POA__get_the_POAManager(poa, &ev), &ev);

  obj = impl_CosNaming_NamingContext__create(poa, NULL, &ev);

  actobj = impl_CosNaming_NamingContext__create(poa, &activation_ctx, &ev);

  CosNaming_NamingContext_bind_context(obj, (gpointer)&activation_ctx_name, actobj, &ev);

  CORBA_Object_release(actobj, &ev);

  memset(&sa, '\0', sizeof(sa));
  sa.sa_handler = (gpointer)&regen_activation_mapping;
  sigaction(SIGHUP, &sa, NULL);
  sa.sa_handler = (gpointer)&doNothing;
  sigaction(SIGALRM, &sa, NULL);
  sa.sa_handler = SIG_IGN;
  sigaction(SIGPIPE, &sa, NULL);

  regen_activation_mapping();

  ior = CORBA_ORB_object_to_string(orb, obj, &ev);

  propname = gdk_atom_intern("CORBA_NAME_SERVICE", FALSE);
  proptype = gdk_atom_intern("STRING", FALSE);
  gdk_property_change(GDK_ROOT_PARENT(), propname, proptype, 8,
		      GDK_PROP_MODE_REPLACE, ior, strlen(ior)+1);
  gdk_flush();

  if(!gdk_property_get(GDK_ROOT_PARENT(), propname, proptype,
		       0, 9999, FALSE, NULL,
		       NULL, &ior_len, (guchar **)&chkior))
    g_error("Couldn't set the property!");

  chkior [ior_len] = 0;
  if(!chkior)
    g_error("Couldn't get the property!");

  if(strcmp(ior, chkior))
    g_error("The property doesn't match!");

  CORBA_free(ior);

  CORBA_Object_release(obj, &ev);

  gtk_main();
#if 0
  CORBA_ORB_run(orb, &ev);
#endif

  return 0;
}

typedef struct {
  GList *removeme;
  GList *addme;
  GString *tmpstr;
} RegenInfo;

static void
regen_create_add_list(impl_POA_CosNaming_NamingContext *srv,
		      RegenInfo *ri,
		      char *config_path)
{
  void *iter;
  char *name, *val;

  iter = gnome_config_init_iterator_sections("/CORBA/Factories");
  while((iter = gnome_config_iterator_next(iter, &name, NULL))) {
    if(!g_hash_table_lookup(srv->registered_names, name)) {
      RegisteredName *newent;

      g_string_sprintf(ri->tmpstr, "/CORBA/Factories/%s/exe", name);
      val = gnome_config_get_string(ri->tmpstr->str);

      g_message("Adding exe mapping for [%s, Factory] -> %s",
		name, val);

      newent = g_new0(RegisteredName, 1);
      newent->id = name;
      newent->kind = g_strdup("Factory");
      newent->type = OBJECT_INACTIVE;
      newent->u.obj.inactive_cmdline = val;

      ri->addme = g_list_prepend(ri->addme, newent);
    } else {
      g_free(name); g_free(val);
    }
  }
}

static void
regen_create_remove_list(char *key, RegisteredName *value, RegenInfo *ri)
{
  char *tmp;
  gboolean used_default;

  g_string_sprintf(ri->tmpstr, "/CORBA/Factories/%s/exe", key);
  tmp = gnome_config_get_string_with_default(ri->tmpstr->str,
					     &used_default);

  if(used_default)
    ri->removeme = g_list_prepend(ri->removeme, value);

  g_free(tmp);
}

static void
regen_activation_mapping(void)
{
  CORBA_Environment ev;
  impl_POA_CosNaming_NamingContext *srv = activation_ctx;
  RegenInfo ri = {NULL, NULL, g_string_new(NULL)};
  GList *item;
  RegisteredName *rn;

  CORBA_exception_init(&ev);

  g_hash_table_foreach(srv->registered_names,
		       (GHFunc)&regen_create_remove_list, &ri);

  regen_create_add_list(srv, &ri, "/CORBA/Factories");
  
  g_assert(srv);
  
  g_string_free(ri.tmpstr, TRUE);

  for(item = ri.addme; item; item = g_list_next(item)) {
    rn = item->data;
    g_hash_table_insert(srv->registered_names, rn->id, rn);
  }
  g_list_free(ri.addme);

  for(item = ri.removeme; item; item = g_list_next(item)) {
    rn = item->data;
    g_hash_table_remove(srv->registered_names, rn->id);
    switch(rn->type) {
    case OBJECT_ACTIVE:
      CORBA_Object_release(rn->u.obj.active_object, &ev);
    case OBJECT_INACTIVE:
      g_free(rn->u.obj.inactive_cmdline);
      break;
    case CTX_REMOTE:
      CORBA_Object_release(rn->u.ctx, &ev);
      break;
    }
    g_free(rn->id); g_free(rn->kind);
    g_free(rn);
  }
  g_list_free(ri.removeme);
}
