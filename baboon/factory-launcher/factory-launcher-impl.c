#include "factory-launcher.h"
#include <libgnorba/name-service.h>
#include <gnome.h>
#include <stdlib.h>
#include <signal.h>

/*** App-specific servant structures ***/
typedef struct {
  POA_FactoryLauncher servant;
  PortableServer_POA poa;

  GHashTable *map;
  CosNaming_NamingContext ns;
} impl_POA_FactoryLauncher;

/*** Implementation stub prototypes ***/
static void impl_FactoryLauncher__destroy(impl_POA_FactoryLauncher * servant,
					  CORBA_Environment * ev);
void
impl_FactoryLauncher_launch(impl_POA_FactoryLauncher * servant,
			    CORBA_char * name,
			    CORBA_Environment * ev);
static void regen_name_mapping(impl_POA_FactoryLauncher *servant);

/*** epv structures ***/
static PortableServer_ServantBase__epv impl_FactoryLauncher_base_epv =
{
  NULL,			/* _private data */
  (gpointer) & impl_FactoryLauncher__destroy,	/* finalize routine */
  NULL,			/* default_POA routine */
};

static POA_FactoryLauncher__epv impl_FactoryLauncher_epv =
{
  NULL,			/* _private */
  (gpointer) & impl_FactoryLauncher_launch,

};

/*** vepv structures ***/
static POA_FactoryLauncher__vepv impl_FactoryLauncher_vepv =
{
  &impl_FactoryLauncher_base_epv,
  &impl_FactoryLauncher_epv,
};

impl_POA_FactoryLauncher newservant = { { NULL, &impl_FactoryLauncher_vepv} };

/*** Stub implementations ***/
static FactoryLauncher 
impl_FactoryLauncher__create(PortableServer_POA poa, CORBA_Environment * ev)
{
  FactoryLauncher retval;
  PortableServer_ObjectId objid = {0, sizeof("FactoryLauncher"), "FactoryLauncher" };

  newservant.servant.vepv = &impl_FactoryLauncher_vepv;
  newservant.poa = poa;
  newservant.map = NULL;

  POA_FactoryLauncher__init((PortableServer_Servant) &newservant, ev);

  regen_name_mapping(&newservant);

  PortableServer_POA_activate_object_with_id(poa, &objid, &newservant, ev);

  retval = PortableServer_POA_servant_to_reference(poa, &newservant, ev);

  return retval;
}

/* You shouldn't call this routine directly without first deactivating the servant... */
static void
impl_FactoryLauncher__destroy(impl_POA_FactoryLauncher * servant, CORBA_Environment * ev)
{
  g_hash_table_destroy(servant->map);
  POA_FactoryLauncher__fini((PortableServer_Servant) servant, ev);
  g_free(servant);
}

void
impl_FactoryLauncher_launch(impl_POA_FactoryLauncher * servant,
			    CORBA_char * name,
			    CORBA_Environment * ev)
{
  CORBA_char *exe;

  exe = g_hash_table_lookup(servant->map, name);

  if(exe)
    system(exe);
}

void
free_entry(gpointer key, gpointer value, gpointer user_data)
{
  g_free(key);
  g_free(value);
}

static void
regen_name_mapping(impl_POA_FactoryLauncher *servant)
{
  void *iter;
  char *name, *exe;

  if(servant->map) {
    g_hash_table_foreach(servant->map, free_entry, NULL);
    g_hash_table_destroy(servant->map);
  }

  servant->map = g_hash_table_new(g_str_hash, g_str_equal);

  for(iter = gnome_config_init_iterator("/CORBA/launchers");
      iter; iter = gnome_config_iterator_next(iter, &name, &exe)) {
    g_hash_table_insert(servant->map, name, exe);
  }
}

void
handle_SIGHUP(void)
{
  regen_name_mapping(&newservant);
}

void
handle_SIGS(void)
{
}

int main(int argc, char *argv[])
{
  struct sigaction sa;
  int i;

  CORBA_Object obj;
  PortableServer_POA poa;
  CORBA_ORB orb;
  CORBA_Environment ev;
  CosNaming_NameComponent mynamepart = {"FactoryLauncher", ""};
  CosNaming_Name myname = {0, 1, &mynamepart };

  if(argc != 2) {
    g_message("Usage: factory-launcher nameserviceIOR");
    return 1;
  }

  memset(&sa, '\0', sizeof(sa));
  sa.sa_handler = (gpointer)&handle_SIGS;
  for(i = 0; i < _NSIG; i++)
    sigaction(i, &sa, NULL);
  sa.sa_handler = (gpointer)&handle_SIGHUP;
  sigaction(SIGHUP, &sa, NULL);

  gnomelib_init("factory-launcher");

  CORBA_exception_init(&ev);

  orb = CORBA_ORB_init(&argc, argv, "orbit-local-orb", &ev);

  obj = impl_FactoryLauncher__create(poa, &ev);

  newservant.ns = CORBA_ORB_string_to_object(orb, argv[1], &ev);

  CosNaming_NamingContext_bind(newservant.ns, &myname, obj, &ev);

  CORBA_Object_release(obj, &ev);

  CORBA_ORB_run(orb, &ev);

  return 0;
}
