#include <libgnorba/gnorba.h>
#include "baboons.h"

CORBA_Object
baboon_get_factory(char *factory_name, CORBA_Environment *ev)
{
  CORBA_Object ns, retval;
  static CosNaming_NameComponent nc[2] =
  {{"GNOMEFactories", "GNOMEFactories"}};
  static const CosNaming_Name n =
  {0, 2, (gpointer)nc};

  ns = gnome_get_name_service();

  if(CORBA_Object_is_nil(ns, ev))
    return CORBA_OBJECT_NIL;

  nc[1].id = nc[1].kind = factory_name;

  retval = CosNaming_NamingContext_resolve(ns, (CosNaming_Name *)&n, ev);

  CORBA_Object_release(ns, ev);

  return retval;
}

CORBA_Object
baboon_Moniker_resolve(baboon_Moniker *moniker, CORBA_Environment *ev)
{
  CORBA_Object retval = CORBA_OBJECT_NIL, factory, ns;
  baboon_Moniker mymoniker;

  static CosNaming_NameComponent nc[2] =
  {{"GNOMEFactories", "GNOMEFactories"}};
  static const CosNaming_Name n =
  {0, 2, (gpointer)nc};

  if(!moniker || !moniker->_length) {
    CORBA_exception_set(ev, CORBA_USER_EXCEPTION, ex_baboon_MonikerResolver_NotFound, NULL);
    return CORBA_OBJECT_NIL;
  }

  if(strcmp(moniker->_buffer[0].kind, "Factory")) {
    CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			ex_baboon_MonikerResolver_NotFound, NULL);
    return CORBA_OBJECT_NIL;
  }

  ns = gnome_get_name_service();

  nc[1].kind = moniker->_buffer[0].kind;
  nc[1].id = moniker->_buffer[0].data;
  factory = CosNaming_NamingContext_resolve(ns, (CosNaming_Name *)&n, ev);
  if(ev->_major)
    return CORBA_OBJECT_NIL;

  mymoniker = *moniker;
  mymoniker._buffer++;
  mymoniker._length--;
  retval = baboon_MonikerResolver_resolve(factory, &mymoniker, ev);

  CORBA_Object_release(ns, ev);

  return retval;
}
