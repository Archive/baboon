#ifndef BABOONS_H
#define BABOONS_H 1

#include <libgnorba/gnorba.h>

#include "baboon.h"

/* Routines for manipulating a StorageFactory local to this process.
   You shouldn't register this with the name service, because each
   process will want its own StorageFactory (i.e. once
   in-process CORBA objects are implemented, it will be much faster
   that way overall)
*/
baboon_StorageFactory
baboon_StorageFactory__create(PortableServer_POA poa,
			      PortableServer_Servant *servant_return,
			      CORBA_Environment *ev);
void
baboon_StorageFactory__destroy(PortableServer_Servant servant,
			       CORBA_Environment *ev);

/* Resolves a moniker into an active Document::Item */
CORBA_Object baboon_Moniker_resolve(baboon_Moniker *moniker, CORBA_Environment *ev);

/* Does a naming service lookup on GNOMEFactories/<factory_name>, and
   returns the result */
CORBA_Object baboon_get_factory(char *factory_name,
				CORBA_Environment *ev);
#endif /* BABOONS_H */
