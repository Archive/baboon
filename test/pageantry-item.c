#include <gdk/gdkx.h>
#include "pageantry.h"

typedef struct {
  baboon_Document_Item objref;
  char *id;
} Document_Container_Item;

/* Implementation of baboon::View::Container */
typedef struct {
  POA_baboon_View_Container servant;
  PortableServer_POA poa;
  PortableServer_ObjectId *objid;

  GtkSocket *socket;
  GnomeCanvasItem *item;
  baboon_View_Contained viewitem;
  /* Points back to the item we're displaying */
  Document_Container_Item *docitem;
} impl_POA_baboon_View_Container;

static CORBA_long
impl_baboon_View_Container__get_wid(impl_POA_baboon_View_Container * servant,
				    CORBA_Environment * ev)
{
  gtk_widget_realize(GTK_WIDGET(servant->socket));

  return GDK_WINDOW_XWINDOW(GTK_WIDGET(servant->socket)->window);
}

static void
impl_baboon_View_Container_activated(impl_POA_baboon_View_Container * servant,
				     baboon_View_Contained viewitem,
				     CORBA_Environment * ev)
{
  servant->viewitem = CORBA_Object_duplicate(viewitem, ev);
}

static void
impl_baboon_View_Container_deactivate(impl_POA_baboon_View_Container * servant,
				      CORBA_Environment * ev)
{
  PortableServer_POA_deactivate_object(servant->poa, servant->objid, ev);
  CORBA_free(servant->objid);

  gtk_object_destroy(GTK_OBJECT(servant->item));
  POA_baboon_View_Container__fini((PortableServer_Servant) servant, ev);

  g_free(servant);
}

static void
baboon_View_Container_destroy(GtkWidget *parent,
			      impl_POA_baboon_View_Container *servant)
{
  CORBA_Environment ev;
  CORBA_exception_init(&ev);

  impl_baboon_View_Container_deactivate(servant, &ev);

  CORBA_exception_free(&ev);
}

static PortableServer_ServantBase__epv impl_baboon_View_Container_base_epv =
{
  NULL,	/* _private data */
  NULL,	/* finalize routine */
  NULL,	/* default_POA routine */
};

static POA_baboon_View_DocElement__epv impl_baboon_View_Container_DocElement_epv =
{
   NULL,			/* _private */
   (gpointer) & impl_baboon_View_Container_deactivate,
};

static POA_baboon_View_Container__epv impl_baboon_View_Container_epv =
{
   NULL, /* _private */
   (gpointer) & impl_baboon_View_Container__get_wid,
   (gpointer) & impl_baboon_View_Container_activated,
};

static POA_baboon_View_Container__vepv impl_baboon_View_Container_vepv =
{
   &impl_baboon_View_Container_base_epv,
   &impl_baboon_View_Container_DocElement_epv,
   &impl_baboon_View_Container_epv,
};

void
do_frame_menu(GtkWidget *frame,
	      GdkEventButton *event,
	      impl_POA_baboon_View_Container *servant)
{
  if(event->button == 3) {
    GtkWidget *menu, *menuitem;

    menu = gtk_menu_new();
    menuitem = gtk_menu_item_new_with_label("Create another view of this item");
    gtk_menu_append(GTK_MENU(menu), menuitem);

    /* blah */

    gtk_widget_destroy(menu);
  }
}

baboon_View_Container 
baboon_View_Container__create(Document_Container_Item *item,
			      GnomeCanvasGroup *parent,
			      PortableServer_POA poa,
			      CORBA_Environment * ev)
{
   baboon_View_Container retval;
   impl_POA_baboon_View_Container *newservant;
   GtkWidget *frame;

   newservant = g_new0(impl_POA_baboon_View_Container, 1);
   newservant->servant.vepv = &impl_baboon_View_Container_vepv;
   newservant->poa = poa;
   POA_baboon_View_Container__init((PortableServer_Servant) newservant, ev);
   newservant->objid = PortableServer_POA_activate_object(poa, newservant, ev);
   newservant->socket = GTK_SOCKET(gtk_socket_new());
   gtk_signal_connect(GTK_OBJECT(newservant->socket), "destroy",
		      baboon_View_Container_destroy,
		      newservant);

   newservant->docitem = item;
   frame = gtk_frame_new(item->id);
   gtk_container_add(GTK_CONTAINER(frame), GTK_WIDGET(newservant->socket));
   gtk_signal_connect(GTK_OBJECT(frame), "button_release_event",
		      do_frame_menu, newservant);

   gtk_widget_show_all(frame);

   newservant->item = gnome_canvas_item_new(GNOME_CANVAS_GROUP(parent),
					    gnome_canvas_widget_get_type(),
					    "widget", frame,
					    "x", 10.0,
					    "y", 10.0,
					    "width", 215.0,
					    "height", 65.0,
					    "size_pixels", FALSE,
					    NULL);

   retval = PortableServer_POA_servant_to_reference(poa, newservant, ev);

   return retval;
}

/*************************************************
 * Implementation of baboon::Document::Container *
 *************************************************/

typedef struct {
  POA_baboon_Document_Container servant;
  PortableServer_POA poa;
  PortableServer_ObjectId *objid;

  GnomeCanvasGroup *cgroup;

  GHashTable *items;
} impl_POA_baboon_Document_Container;

void
get_items_iterator(gpointer key,
		   Document_Container_Item *value,
		   baboon_stringlist *setme)
{
  setme->_buffer[setme->_length] = CORBA_string_dup(value->id);
  setme->_length++;
}

baboon_stringlist *
impl_baboon_Document_Container__get_items
(impl_POA_baboon_Document_Container* servant,
 CORBA_Environment * ev)
{
  baboon_stringlist *retval;

  retval = baboon_stringlist__alloc();
  retval->_length = g_hash_table_size(servant->items);
  retval->_buffer = CORBA_sequence_CORBA_string_allocbuf(retval->_length);
  CORBA_sequence_set_release(retval, CORBA_TRUE);

  retval->_length = 0; /* It gets incremented by the iterator */

  g_hash_table_foreach(servant->items, (GHFunc)&get_items_iterator, retval);

  return retval;
}

baboon_Document_Item
impl_baboon_Document_Container_get_item
(impl_POA_baboon_Document_Container * servant,
 CORBA_char * id,
 CORBA_Environment * ev)
{
  baboon_Document_Item retval;
  
  g_return_val_if_fail(id && !*id, CORBA_OBJECT_NIL);

  retval = g_hash_table_lookup(servant->items, id);
  
  return CORBA_Object_duplicate(retval, ev);
}

void
impl_baboon_Document_Container_add_item
(impl_POA_baboon_Document_Container * servant,
 baboon_Document_Item newitem,
 CORBA_Environment * ev)
{
  Document_Container_Item *newdocitem;
  GtkWidget *get_id_dlg, *get_id, *get_id_box;
  char buf[32];
  int i;
  CORBA_Object view_container;

  for(i = g_hash_table_size(servant->items); ; i++) {
    g_snprintf(buf, sizeof(buf), "Item%d", i);
    if(!g_hash_table_lookup(servant->items, buf))
      break;
  }

  get_id_dlg = gnome_dialog_new(gnome_app_id, "Continue", "Cancel", NULL);

  /* FIXME: this function no longer exists */
  get_id_box = gnome_build_labelled_widget("Item Name",
					   (get_id = gtk_entry_new()));
  gtk_widget_show(get_id_box);

  gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(get_id_dlg)->vbox),
		    get_id_box);

  gtk_entry_set_text(GTK_ENTRY(get_id), buf);

  i = gnome_dialog_run_and_hide(GNOME_DIALOG(get_id_dlg));

  if(0 != i
     || g_hash_table_lookup(servant->items,
			    gtk_entry_get_text(GTK_ENTRY(get_id)))) {
    CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			ex_baboon_Document_Container_ItemRejected,
			NULL);
    return;
  }

  newdocitem = g_new(Document_Container_Item, 1);

  newdocitem->id = g_strdup(gtk_entry_get_text(GTK_ENTRY(get_id)));
  gtk_object_destroy(GTK_OBJECT(get_id_dlg));

  newdocitem->objref = CORBA_Object_duplicate(newitem, ev);

  view_container = baboon_View_Container__create(newdocitem,
						 servant->cgroup,
						 servant->poa,
						 ev);

  baboon_Document_Item_display(newitem, view_container, ev);
  CORBA_Object_release(view_container, ev);

  g_hash_table_insert(servant->items, newdocitem->id, newdocitem);
}

void
impl_baboon_Document_Container_add_item_by_moniker(impl_POA_baboon_Document_Container * servant,
						   baboon_Moniker * itempath,
						   CORBA_Environment * ev)
{
  CORBA_Object new_item;

  new_item = baboon_Moniker_resolve(itempath, ev);

  if(ev->_major != CORBA_NO_EXCEPTION)
    return;

  impl_baboon_Document_Container_add_item(servant, new_item, ev);
}

static PortableServer_ServantBase__epv impl_baboon_Document_Container_base_epv =
{
   NULL, /* _private data */
   NULL, /* finalize routine */
   NULL, /* default_POA routine */
};

static POA_baboon_Document_Container__epv impl_baboon_Document_Container_epv =
{
   NULL,			/* _private */
   (gpointer) &impl_baboon_Document_Container__get_items,
   (gpointer)&impl_baboon_Document_Container_get_item,
   (gpointer)&impl_baboon_Document_Container_add_item_by_moniker,
   (gpointer)&impl_baboon_Document_Container_add_item,
};

static POA_baboon_Document_Container__vepv impl_baboon_Document_Container_vepv =
{
   &impl_baboon_Document_Container_base_epv,
   &impl_baboon_Document_Container_epv,
};

GtkWidget * /* A viewport for this container */
pageantry_create_container(PortableServer_POA poa, CORBA_Environment * ev)
{
  GtkWidget *area;
  impl_POA_baboon_Document_Container *newservant;
  PortableServer_ObjectId *objid;

  newservant = g_new0(impl_POA_baboon_Document_Container, 1);
  newservant->servant.vepv = &impl_baboon_Document_Container_vepv;
  newservant->poa = poa;
  newservant->items = g_hash_table_new(g_str_hash, g_str_equal);

  POA_baboon_Document_Container__init((PortableServer_Servant) newservant, ev);

  objid = PortableServer_POA_activate_object(poa, newservant, ev);
  newservant->objid = objid;

#if 0
  newservant->me = PortableServer_POA_servant_to_reference(poa, newservant, ev);
#endif

  gtk_widget_push_visual(gdk_imlib_get_visual());        
  gtk_widget_push_colormap(gdk_imlib_get_colormap());
  area = gnome_canvas_new();
  gtk_widget_pop_visual();
  gtk_widget_pop_colormap();
  gtk_widget_set_usize(area, 500, 500);
  gnome_canvas_set_scroll_region(GNOME_CANVAS(area), 0, 0, 500, 500);

  gtk_object_set_data(GTK_OBJECT(area), "Document::Container",
		      newservant);

  newservant->cgroup = GNOME_CANVAS_GROUP(gnome_canvas_root(GNOME_CANVAS(area)));

  return area;
}

typedef struct {
  baboon_Moniker path;
} CTreeFactoryInfo;

CTreeFactoryInfo *factory_info_new(char *config_path)
{
  CTreeFactoryInfo *retval;
  char **parts;
  char *t;
  int i;

  retval = g_new(CTreeFactoryInfo, 1);
  parts = g_strsplit(config_path, "/", -1);
  for(i = 0; parts[i]; i++) /* count number of parts */ ;
  retval->path._length = i - 3; /* A split of "/CORBA/Factories/Foo"
				   will give four elements - we
				   discard the first three */
  retval->path._buffer = CORBA_sequence_baboon_MonikerElement_allocbuf(retval->path._length);

  for(i = 3; parts[i]; i++) {
    if(parts[i+1]) {
      t = g_strjoin(parts[i], "_List", NULL);
      retval->path._buffer[i - 3].data = CORBA_string_dup(t);
      g_free(t);
    } else
      retval->path._buffer[i - 3].data = CORBA_string_dup(parts[i]);
    retval->path._buffer[i - 3].kind =
      (parts[i+1])?CORBA_string_dup("Context"):CORBA_string_dup("Factory");
  }

  g_strfreev(parts);

  return retval;
}

void factory_info_free(CTreeFactoryInfo *fi)
{
  CORBA_free(fi->path._buffer);
  g_free(fi);
}

/**** create_factory_list
      Inputs: 'ctree' - a newly created GtkCTree widget
      Side effects: Adds nodes into 'ctree'

      Description: Goes through all the factories registered with gnome_config,
      and generates a tree, from which the user can select the type of
      document they wish to create.

      Notes: Calls itself recursively to generate branches of the tree.
 */
void
create_factory_list(GtkCTree *ctree,
		    char *config_path,
		    GtkCTreeNode *parent)
{
  void *iter, *subsect_iter;
  GString *tmpstr = g_string_new(NULL);
  GtkCTreeNode *node = NULL;
  char *cursection, *subsection, *name, *description;
  char *cols[3];
  gboolean has_subsection;
  CTreeFactoryInfo *info;

  iter = gnome_config_init_iterator_sections(config_path);
  cols[0] = NULL;

  while((iter = gnome_config_iterator_next(iter, &cursection, NULL))) {
    g_string_sprintf(tmpstr, "%s/%s", config_path, cursection);

    has_subsection = FALSE;

    subsect_iter = gnome_config_init_iterator_sections(tmpstr->str);
    while((subsect_iter = gnome_config_iterator_next(subsect_iter,
						     &subsection, NULL))) {
      /* This is a way of finding out whether the current entry has
	 subsections */
      has_subsection = TRUE;
      g_free(subsection);
    }

    g_string_sprintf(tmpstr, "%s/%s/name", config_path, cursection);
    name = gnome_config_get_string(tmpstr->str);
    g_string_sprintf(tmpstr, "%s/%s/description", config_path, cursection);
    description = gnome_config_get_string(tmpstr->str);

    cols[1] = name;
    cols[2] = description;

    node = gtk_ctree_insert_node(ctree, parent, node, cols, 5,
				 NULL, NULL, NULL, NULL, has_subsection,
				 FALSE);
    
    g_string_sprintf(tmpstr, "%s/%s", config_path, cursection);
    info = factory_info_new(tmpstr->str);

    gtk_ctree_node_set_row_data_full(ctree, node, info,
				     (GtkDestroyNotify)&factory_info_free);

    subsect_iter = gnome_config_init_iterator_sections(tmpstr->str);
    while((subsect_iter = gnome_config_iterator_next(subsect_iter,
						     &subsection, NULL))) {
      if(!strcmp(subsection, "name")
	 || !strcmp(subsection, "description"))
	continue;

      /* Now we create the tree for all the subsections of this
	 current factory section */

      g_string_sprintf(tmpstr, "%s/%s/%s", config_path, cursection, subsection);
      create_factory_list(ctree, tmpstr->str, node);
      g_free(subsection);
    }

    g_free(name);
    g_free(description);
    g_free(cursection);
  }

  g_string_free(tmpstr, TRUE);
}

void cb_pageantry_add_item(GtkWidget *widget, GtkWidget *area)
{
  baboon_Document_Item additem;
  int res;
  CTreeFactoryInfo *selected_item;
  GtkCTreeNode *selected_node;
  CORBA_Environment ev;

  GtkWidget *choosedialog, *ctree;

  /* TODO here: Find out what item the user wants, create a new
     object for it, and then create a view for that object to display into */

  choosedialog = gnome_dialog_new("Choose new document type",
				  "Create", "Cancel", NULL);

  ctree = gtk_ctree_new(3, 0); /* Three columns, the first one being for
				  the tree, the other two being for document
				  type names & descriptions */

  create_factory_list(GTK_CTREE(ctree), "/CORBA/Factories", NULL);

  gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(choosedialog)->vbox), ctree);

  gtk_widget_show_all(choosedialog);

  res = gnome_dialog_run_and_hide(GNOME_DIALOG(choosedialog));

  if(!GTK_CLIST(ctree)->selection)
    goto out;
  
  /* Now that they've made a selection, try to fire it up... */
  selected_node = GTK_CLIST(ctree)->selection->data;
  selected_item = gtk_ctree_node_get_row_data(GTK_CTREE(ctree), selected_node);
  gtk_ctree_node_set_row_data(GTK_CTREE(ctree), selected_node, NULL);

  CORBA_exception_init(&ev);

  additem = baboon_Moniker_resolve(&selected_item->path,
				   &ev);

  if(ev._major != CORBA_NO_EXCEPTION || CORBA_Object_is_nil(additem, &ev)) {
    g_warning("Failed to resolve moniker");
    goto out;
  }

  impl_baboon_Document_Container_add_item(gtk_object_get_data(GTK_OBJECT(area),
							      "Document::Container"),
					  additem, &ev);

 out:
  gtk_widget_destroy(choosedialog);

  CORBA_exception_free(&ev);
}

void
cb_pageantry_save_page(GtkWidget *widget, GtkWidget *area)
{
}
