#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <baboon/baboons.h>
#include <vfs.h>

typedef struct {
  POA_baboon_Document_ItemFactory servant;
  PortableServer_POA poa;
  PortableServer_ObjectId *objid;
} impl_POA_baboon_Document_ItemFactory;

typedef struct {
  POA_baboon_Document_Item servant;
  PortableServer_POA poa;
  PortableServer_ObjectId *objid;

  baboon_Moniker attr_activation_path;
  baboon_Timestamp attr_last_modified;
  GList *views;
  char *contents;
  CORBA_short attr_undolevels;
  CORBA_boolean in_update;
} impl_POA_baboon_Document_Item;


typedef struct {
  POA_baboon_View_Contained servant;
  PortableServer_POA poa;
  PortableServer_ObjectId *objid;

  impl_POA_baboon_Document_Item *item;
  GtkWidget *plug, *editme;

  baboon_View_Container parent;
} impl_POA_baboon_View_Contained;

static PortableServer_ServantBase__epv base_epv =
{
  NULL,			/* _private data */
  NULL, /* finalize */
  NULL	/* default_POA routine */
};

/* View::Contained stuff */
void
impl_baboon_View_Contained_deactivate(impl_POA_baboon_View_Contained * servant,
				      CORBA_Environment * ev)
{
  if(servant->plug)
    gtk_widget_destroy(servant->plug);

  PortableServer_POA_deactivate_object(servant->poa, servant->objid, ev);
  POA_baboon_View_Contained__fini((PortableServer_Servant)servant, ev);

  CORBA_Object_release(servant->parent, ev);
  servant->item->views = g_list_remove(servant->item->views, servant);

  CORBA_free(servant->objid);

  g_free(servant);
}

static gboolean
baboon_View_Contained_destroy_event(GtkWidget *plug,
				    impl_POA_baboon_View_Contained *servant)
{
  CORBA_Environment ev;

  CORBA_exception_init(&ev);

  servant->plug = NULL;
  impl_baboon_View_Contained_deactivate(servant, &ev);

  CORBA_exception_free(&ev);

  g_message("handled destroy");

  return FALSE;
}

/* View::Contained implementation */
static POA_baboon_View_DocElement__epv impl_baboon_View_DocElement_epv =
{
  NULL,			/* _private */
  (gpointer) & impl_baboon_View_Contained_deactivate,
};

static POA_baboon_View_Contained__epv impl_baboon_View_Contained_epv =
{
  NULL,			/* _private */
};

static POA_baboon_View_Contained__vepv impl_baboon_View_Contained_vepv =
{
  &base_epv,
  &impl_baboon_View_DocElement_epv,
  &impl_baboon_View_Contained_epv,
};

static gboolean
baboon_View_Contained_destroy_event(GtkWidget *plug,
				   impl_POA_baboon_View_Contained *servant);

baboon_Moniker *
impl_baboon_Document_Item__get_activation_path(impl_POA_baboon_Document_Item * servant,
					       CORBA_Environment * ev)
{
  baboon_Moniker *retval;
  
  retval = baboon_Moniker__alloc();

  retval->_length = servant->attr_activation_path._length;
  retval->_buffer = servant->attr_activation_path._buffer;
  CORBA_sequence_set_release(retval, CORBA_FALSE);

  return retval;
}

/**** baboon_Document_Item_update_views
      Inputs: 'text' - a GtkText widget that is displaying a Document_Item
              'servant' - the document item being edited
      Side effects: modifies 'servant->contents'

      Called: When the 'changed' signal is sent to a GtkText

      Description: When a the GtkText (that implements a view of a
                   Document Item) is edited, we want to publish the
                   changes to other GtkText's that are displaying the
                   same item. We go through them all and update their
                   contents. While doing this loop, we set the
		   'in_update' flag in the docitem in order to make
		   sure that the 'changed' signals that we generate
		   by updating the other GtkText's do not start
		   bouncing around forever :)

*/
void
baboon_Document_Item_update_views(GtkWidget *text,
				  impl_POA_baboon_Document_Item *servant)
{
  GList *item;
  impl_POA_baboon_View_Contained *aview;
  int content_len, itmp;

  /* The 'in_update' flag lets us avoid paying attention to the
     "changed" signal if it is created by us updating other views */
  if(servant->in_update)
    return;
  servant->in_update = TRUE;

  g_free(servant->contents);
  content_len = gtk_text_get_length(GTK_TEXT(text));
  servant->contents = gtk_editable_get_chars(GTK_EDITABLE(text),
					     0, content_len);

  for(item = servant->views; item; item = g_list_next(item)) {
    aview = item->data;

    if(aview->editme == text)
      continue;

    gtk_editable_delete_text(GTK_EDITABLE(aview->editme), 0, content_len);
    gtk_editable_insert_text(GTK_EDITABLE(aview->editme), servant->contents,
			     content_len, &itmp);
  }

  servant->in_update = FALSE;
}

void
impl_baboon_Document_Item_display(impl_POA_baboon_Document_Item * servant,
				  baboon_View_Container in_view,
				  CORBA_Environment * ev)
{
  /* The "create a View::Contained" routine */
  baboon_View_Contained showme;
  impl_POA_baboon_View_Contained *newservant;
  CORBA_unsigned_long wid;
  int itmp;

  newservant = g_new0(impl_POA_baboon_View_Contained, 1);
  newservant->servant.vepv = &impl_baboon_View_Contained_vepv;
  POA_baboon_View_Contained__init((PortableServer_Servant) newservant, ev);
  newservant->objid = PortableServer_POA_activate_object(servant->poa, newservant, ev);

  newservant->poa = servant->poa;
  newservant->parent = CORBA_Object_duplicate(in_view, ev);
  newservant->item = servant;

  if(ev->_major != CORBA_NO_EXCEPTION)
    goto out;

  wid = baboon_View_Container__get_wid(in_view, ev);
  if(ev->_major != CORBA_NO_EXCEPTION)
    goto out;

  newservant->editme = gtk_text_new(NULL, NULL);
  newservant->plug = gtk_plug_new(wid);

  gtk_signal_connect(GTK_OBJECT(newservant->plug), "destroy",
		     GTK_SIGNAL_FUNC(baboon_View_Contained_destroy_event),
		     newservant);

  gtk_container_add(GTK_CONTAINER(newservant->plug), newservant->editme);
  gtk_widget_show_all(newservant->plug);

  servant->views = g_list_prepend(servant->views, newservant);
  gtk_editable_insert_text(GTK_EDITABLE(newservant->editme),
			   servant->contents,
			   strlen(servant->contents), &itmp);
  gtk_signal_connect(GTK_OBJECT(newservant->editme), "changed",
		     baboon_Document_Item_update_views, servant);

  gtk_widget_set_usize(newservant->editme, 200, 50);
  gtk_text_set_editable(GTK_TEXT(newservant->editme), TRUE);

  showme = PortableServer_POA_servant_to_reference(servant->poa, newservant, ev);

  baboon_View_Container_activated(in_view, showme, ev);

  CORBA_Object_release(showme, ev);
 out:
}

baboon_Data *
impl_baboon_Document_Item_renderToPS(impl_POA_baboon_Document_Item * servant,
				     CORBA_Environment * ev)
{
  baboon_Data *retval;

  retval = baboon_Data__alloc();
  retval->_length = 0;
  retval->_buffer = NULL;

  CORBA_sequence_set_release(retval, CORBA_FALSE);

  g_warning("renderToPS NYI");

  return retval;
}

void
impl_baboon_Document_Item_save(impl_POA_baboon_Document_Item * servant,
			       CORBA_Environment * ev)
{
  int fd;

  fd = mc_open(servant->attr_activation_path._buffer[0].data, O_WRONLY);

  if(fd < 0)
    goto error;

  if(mc_write(fd, servant->contents, strlen(servant->contents)) <= 0)
    goto error;

  mc_close(fd);

  return;

 error:
  CORBA_exception_set(ev, CORBA_USER_EXCEPTION, ex_baboon_OpFailed, NULL);
}

void
impl_baboon_Document_Item_undo(impl_POA_baboon_Document_Item * servant,
			       CORBA_Environment * ev)
{
  CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
		      ex_baboon_Document_UnsupportedFeature, NULL);
}

CORBA_short
impl_baboon_Document_Item__get_undolevels(impl_POA_baboon_Document_Item * servant,
					  CORBA_Environment * ev)
{
  return servant->attr_undolevels;
}

void
impl_baboon_Document_Item__set_undolevels(impl_POA_baboon_Document_Item * servant,
					  CORBA_short value,
					  CORBA_Environment * ev)
{
  g_warning("No, we can't set undolevels to %hd", value);
}

baboon_Timestamp
impl_baboon_Document_Item__get_last_modified(impl_POA_baboon_Document_Item * servant,
					     CORBA_Environment * ev)
{
  return servant->attr_last_modified;
}


/* Document::Item stuff */
static POA_baboon_Document_Item__epv impl_baboon_Document_Item_epv =
{
  NULL,			/* _private */
  (gpointer)&impl_baboon_Document_Item__get_activation_path,
  (gpointer)&impl_baboon_Document_Item_display,
  (gpointer)&impl_baboon_Document_Item_renderToPS,
  (gpointer)&impl_baboon_Document_Item_save,
  (gpointer)&impl_baboon_Document_Item_undo,
  (gpointer)&impl_baboon_Document_Item__get_undolevels,
  (gpointer)&impl_baboon_Document_Item__set_undolevels,
  (gpointer)&impl_baboon_Document_Item__get_last_modified,
};

static POA_baboon_Document_Item__vepv impl_baboon_Document_Item_vepv =
{
  &base_epv,
  &impl_baboon_Document_Item_epv,
};

/****************************************
 * Document::ItemFactory                *
 ****************************************/
baboon_Document_Item
impl_baboon_Document_ItemFactory_create_new_item(impl_POA_baboon_Document_ItemFactory * servant,
						 baboon_DataType document_type,
						 CORBA_Environment * ev)
{
  baboon_Document_Item retval = CORBA_OBJECT_NIL;

  CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
		      ex_baboon_MonikerResolver_NotFound,
		      NULL);

  return retval;
}

CORBA_Object
impl_baboon_ItemFactory_resolve(impl_POA_baboon_Document_ItemFactory * servant,
				baboon_Moniker * opath,
				CORBA_Environment * ev)
{
  /* Aka Document_Item__create (more or less */
  baboon_Document_Item retval = CORBA_OBJECT_NIL;
  impl_POA_baboon_Document_Item *newservant;
  GString *tmpstr;
  char buf[1025];
  int fd, readlen;
  gboolean from_file;

#if 0
  if(opath->_length != 1
     || strcmp(opath->_buffer[0].kind, "Filename")) {
    CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			ex_baboon_MonikerResolver_NotFound,
			NULL);
    return retval;
  }
#endif

  newservant = g_new0(impl_POA_baboon_Document_Item, 1);
  newservant->servant.vepv = &impl_baboon_Document_Item_vepv;
  newservant->poa = servant->poa;
  newservant->attr_activation_path = *opath;
  if(opath->_length > 0) {
    newservant->attr_activation_path._buffer =
      CORBA_sequence_baboon_MonikerElement_allocbuf(opath->_length);
    newservant->attr_activation_path._buffer[0].kind =
      CORBA_string_dup(opath->_buffer[0].kind);
    newservant->attr_activation_path._buffer[0].data =
      CORBA_string_dup(opath->_buffer[0].data);
    from_file = TRUE;
  } else {
    newservant->attr_activation_path._buffer = NULL;
    from_file = FALSE;
  }
  CORBA_sequence_set_release(&newservant->attr_activation_path, CORBA_TRUE);

  newservant->attr_undolevels = -1;
  newservant->attr_last_modified = 0;

  tmpstr = g_string_new("");

  if(from_file) {
    fd = mc_open(opath->_buffer[0].data, O_RDONLY);
    while((readlen = mc_read(fd, buf, sizeof(buf) - 1)) > 0) {
      buf[readlen] = '\0';
      g_string_append(tmpstr, buf);
    }
    mc_close(fd);
  }

  newservant->contents = tmpstr->str;
  g_string_free(tmpstr, FALSE);

  POA_baboon_Document_Item__init((PortableServer_Servant) newservant, ev);
  newservant->objid = PortableServer_POA_activate_object(servant->poa,
							 newservant, ev);

  retval = PortableServer_POA_servant_to_reference(servant->poa, newservant, ev);

  return retval;
}

static POA_baboon_MonikerResolver__epv impl_baboon_MonikerResolver_epv =
{
  NULL,			/* _private */
  (gpointer) & impl_baboon_ItemFactory_resolve,
};


static POA_baboon_Document_ItemFactory__epv
impl_baboon_Document_ItemFactory_epv = {
  NULL,			/* _private */
  (gpointer) & impl_baboon_Document_ItemFactory_create_new_item,
};

static POA_baboon_Document_ItemFactory__vepv
impl_baboon_Document_ItemFactory_vepv = {
  &base_epv,
  &impl_baboon_MonikerResolver_epv,
  &impl_baboon_Document_ItemFactory_epv,
};

static baboon_Document_ItemFactory 
baboon_Document_ItemFactory__create(PortableServer_POA poa, CORBA_Environment * ev)
{
  baboon_Document_ItemFactory retval;
  impl_POA_baboon_Document_ItemFactory *newservant;

  newservant = g_new0(impl_POA_baboon_Document_ItemFactory, 1);
  newservant->servant.vepv = &impl_baboon_Document_ItemFactory_vepv;
  newservant->poa = poa;
  POA_baboon_Document_ItemFactory__init((PortableServer_Servant) newservant, ev);
  newservant->objid = PortableServer_POA_activate_object(poa, newservant, ev);

  retval = PortableServer_POA_servant_to_reference(poa, newservant, ev);

  return retval;
}

int main(int argc, char *argv[])
{
  CORBA_Environment ev;
  CORBA_ORB orb;
  PortableServer_POA poa;
  static CosNaming_NameComponent nc[2] =
  {{"GNOMEFactories", "GNOMEFactories"},
   {"TextFactory", "Factory"}};
  static const CosNaming_Name n =
  {0, 2, (gpointer)nc};
  CORBA_Object factory, ns;

  CORBA_exception_init(&ev);

  orb = gnome_CORBA_init("text-item", NULL, &argc, argv,
			 0, NULL, &ev);

  poa = (PortableServer_POA)
    CORBA_ORB_resolve_initial_references(orb, "RootPOA", &ev);
  PortableServer_POAManager_activate(PortableServer_POA__get_the_POAManager(poa, &ev), &ev);

  ns = gnome_get_name_service();

  factory = baboon_Document_ItemFactory__create(poa, &ev);

  CosNaming_NamingContext_rebind(ns, (gpointer)&n, factory, &ev);

  gtk_main();

  CosNaming_NamingContext_unbind(ns, (gpointer)&n, &ev);

  CORBA_Object_release(factory, &ev);
  CORBA_Object_release(ns, &ev);

  return 0;
}
