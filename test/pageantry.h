#ifndef PAGEANTRY_H
#define PAGEANTRY_H 1

#include <gnome.h>
#include <baboon/baboons.h>

GtkWidget *pageantry_create_container(PortableServer_POA poa,
				      CORBA_Environment * ev);
void cb_pageantry_add_item(GtkWidget *widget, GtkWidget *appwin);
void cb_pageantry_save_page(GtkWidget *widget, GtkWidget *area);

extern GtkWidget *view_vbox;

#endif
