#include "pageantry.h"

#define APPID "pageantry"
#define APPNAME "PAGEantry"

static 

GnomeUIInfo progmenu[] = {
  GNOMEUIINFO_ITEM_STOCK("Save", "Save this page", cb_pageantry_save_page,
			 GNOME_STOCK_MENU_SAVE),
  GNOMEUIINFO_ITEM_STOCK("Exit", "Exit this program", gtk_main_quit,
			 GNOME_STOCK_MENU_QUIT),
  GNOMEUIINFO_END
};

GnomeUIInfo itemmenu[] = {
  GNOMEUIINFO_ITEM_STOCK("Create item...", "Add a new document item to this page",
			 cb_pageantry_add_item, GNOME_STOCK_MENU_NEW),
  GNOMEUIINFO_END
};

GnomeUIInfo mainmenu[] = {
  GNOMEUIINFO_SUBTREE("Program", progmenu),
  GNOMEUIINFO_SUBTREE("Item", itemmenu),
  GNOMEUIINFO_END
};

void delete_event (GtkWidget *widget, gpointer *data);

GtkWidget *view_vbox = NULL;

int main(int argc, char *argv[])
{
  GtkWidget *mainwin, *frame, *area;
  CORBA_Environment ev;
  CORBA_ORB orb;
  PortableServer_POA poa;

  CORBA_exception_init(&ev);

  orb = gnome_CORBA_init(APPID, NULL, &argc, argv,
			 0, NULL, &ev);

  poa = (PortableServer_POA)
    CORBA_ORB_resolve_initial_references(orb, "RootPOA", &ev);
  PortableServer_POAManager_activate(PortableServer_POA__get_the_POAManager(poa, &ev), &ev);

  mainwin = gnome_app_new(APPID, APPNAME);
  gtk_signal_connect(GTK_OBJECT(mainwin), "delete_event", delete_event, NULL);

  area = pageantry_create_container(poa, &ev);
  gtk_object_set_data(GTK_OBJECT(mainwin), "page", area);

  frame = gtk_frame_new("Page");
  gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);
  gtk_container_add(GTK_CONTAINER(frame), area);
  gtk_container_border_width(GTK_CONTAINER(frame), 4);
  gnome_app_set_contents(GNOME_APP(mainwin), frame);

  gnome_app_create_menus_with_data(GNOME_APP(mainwin), mainmenu, area);
  gtk_object_set_data(GTK_OBJECT(mainwin), "CORBA_ORB", orb);

  gtk_widget_show_all(mainwin);

  gtk_main();

  return 0;
}

void delete_event (GtkWidget *widget, gpointer *data)
{
  gtk_main_quit();   
}
